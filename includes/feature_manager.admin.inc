<?php
/**
 *
 *
 * @author hairQles
 */

/**
 * admin/structure/feature-manager callback.
 */
function feature_manager_admin_form($form, $form_state) {

}

/**
 * admin/structure/feature-manager/create callback.
 */
function feature_manager_create_form($form, $form_state) {
  module_load_include('inc', 'features', 'features.admin');

  $form['name'] = array(
    '#title' => t('Name'),
    '#description' => t('Example: Image gallery'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#attributes' => array('class' => array('feature-name')),
  );

  $form['module_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine-readable name'),
    '#description' => t('Example: image_gallery') . '<br/>' . t('May only contain lowercase letters, numbers and underscores. <strong>Try to avoid conflicts with the names of existing Drupal projects.</strong>'),
    '#attributes' => array('class' => array('feature-module-name')),
    '#machine_name' => array(
      'source' => array('name'),
      'exists' => 'features_export_form_validate_field',
    ),
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#description' => t('Provide a short description of what users should expect when they enable your feature.'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );

  $form['version'] = array(
    '#title' => t('Version'),
    '#description' => t('Examples: 7.x-1.0, 7.x-1.0-beta1'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#size' => 30,
    '#element_validate' => array('features_export_form_validate_field'),
  );

  $form['project_status_url'] = array(
    '#title' => t('URL of update XML'),
    '#description' => t('Example: http://mywebsite.com/fserver'),
    '#type' => 'textfield',
    '#required' => FALSE,
    '#size' => 30,
    '#element_validate' => array('features_export_form_validate_field'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Edit feature'),
  );

  return $form;
}

/**
 * admin/structure/feature-manager/%feature/edit callback.
 */
function feature_manager_edit_form($form, $form_state, $feature_name = NULL) {
  module_load_include('inc', 'features', 'features.export');
  //load the feature
  $feature = feature_load($feature_name);
//  $features[] = $feature->name;

//  $kaka = features_get_component_states($features);
//  var_dump($features);
//  die();


//if we are creating new feature load it from to ctools cache
  if (empty($feature)) {
    $feature = feature_manager_cache_get($feature_name);
    $feature->isnew = TRUE;
  }

  $form['menu'] = array(
    '#type' => 'actions',
    '#weight' => 1,
  );

  $form['menu']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  $form['menu']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );

  $form['menu']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['board'] = array(
    '#markup' => theme('feature_manager_edit_board', array('feature' => $feature)),
    '#weight' => 5,
  );

  return $form;
}

/**
 * Submit handler for feature_manager_create_form
 */
function feature_manager_create_form_submit($form, &$form_state) {
  //set the temporary feature object
  $feature = new stdClass();
  $feature->name = $form_state['values']['name'];
  $feature->module_name = $form_state['values']['module_name'];
  $feature->description = $form_state['values']['description'];
  $feature->version = $form_state['values']['version'];
  $feature->project_status_url = $form_state['values']['project_status_url'];

  //save the temporary feature in cache
  feature_manager_cache_set($feature);

  $form_state['redirect'] = array("admin/structure/feature-manager/$feature->module_name/edit");
}

/**
 *
 * @param type $element
 */
function feature_manager_edit_element($element) {

}