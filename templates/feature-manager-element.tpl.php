<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="feature-manager-element">
  <div class="contextual-links-region">
    <h2 class="section-title"><?php print $type ?></h2>

    <?php print render($contextual_links); ?>

    <div class="feature-manager-element-body">
      <?php print render($elements) ?>
    </div>
  </div>
</div>